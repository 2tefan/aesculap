```mermaid
graph TD

  subgraph "TODOs"
  Node1[Create TODO-List] -.-> Node2[Add items]
  Node2 --> Node3[Finish TODO-Objective]
  Node3 --> Node2
end
```

**TODO-List**

1. [x] Create TODO-List
1. [ ] Car handling
   1. [x] Steering
   1. [x] Accelerating
   1. [x] Braking
   1. [x] Reversing
   1. [ ] Improve handling