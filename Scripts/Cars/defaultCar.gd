extends VehicleBody

export var MAX_ENGINE_FORCE = 500
export var MAX_BRAKE_FORCE = 20.0
export var MAX_STEER_ANGLE = 0.3

export var MAX_STEERING_SPEED = 0.001
export var MAX_RESET_STEERING_SPEED = 0.003

export var CAR_VELOCITY_TOLERANCE = .4

export var GEARS_COUNT = 5

var TOLERENCE_STEERING = 0.001

var steer_val = 0
var throttle_val = 0
var brake_val = 0

func _ready():
	pass # Replace with function body.

func _physics_process(delta):

	throttle_val = 0
	brake_val = 0
	if Input.is_action_pressed("game_forward"):
		throttle_val = 1.0
	if Input.is_action_pressed("game_back"):
		if get_velocity() > 0:
			brake_val = 1.0
		else:
			brake_val = 0
			throttle_val = -1.0

	if Input.is_action_pressed("game_left"):
		if steer_val < MAX_STEER_ANGLE:
			steer_val += MAX_STEERING_SPEED
	elif Input.is_action_pressed("game_right"):
		if steer_val > -MAX_STEER_ANGLE:
			steer_val -= MAX_STEERING_SPEED
	else:
		if steer_val < TOLERENCE_STEERING:
			steer_val += MAX_RESET_STEERING_SPEED
		elif steer_val > -TOLERENCE_STEERING:
			steer_val -= MAX_RESET_STEERING_SPEED
		else:
			steer_val = 0
	
	if Input.is_action_pressed("game_reset"):
		reset_car()
	
	engine_force = throttle_val * MAX_ENGINE_FORCE
	brake = brake_val * MAX_BRAKE_FORCE

	set_brake(brake)
	set_engine_force(engine_force)
	set_steering(steer_val)

func get_velocity():

	var car_velocity = sqrt(square(linear_velocity.x)+square(linear_velocity.z))
	if car_velocity < CAR_VELOCITY_TOLERANCE:
		car_velocity = 0

	if get_engine_force() < 0:
		car_velocity *= -1

	return car_velocity

func square(var value):
	return value*value

func is_in_contact():
	return $FL.is_in_contact() or $FR.is_in_contact() or $BL.is_in_contact() or $BR.is_in_contact()

func reset_car():
	linear_velocity = Vector3(0,0,0)
	rotation = Vector3(0,0,0)
	angular_velocity = Vector3(0,0,0)
	translation = Vector3(0,3,0)
	$CameraPivot/Camera.camera_reset()