extends Camera

onready var pivot = get_parent()
onready var car = pivot.get_parent()
export var SPEED_CAMERA_MOVEMENT = 1.5
export var AUTO_SPEED_CAMERA_MOVEMENT = 2
export var CAR_STEERING_OFFSET = 60
export var CAR_STEERING_OFFSET_MAX = .5
var wanted_camera_rotation = 0

var car_velocity
var wanted_fov = 80
export var MAX_FOV_STEPS = 0.2

func _process(delta):
	
	car_velocity = car.get_velocity()
	
	if Input.is_action_pressed("ui_left"):
		pivot.rotate_y(SPEED_CAMERA_MOVEMENT*delta)
	elif Input.is_action_pressed("ui_right"):
		pivot.rotate_y(-SPEED_CAMERA_MOVEMENT*delta)
	elif car_velocity != 0 and (car.is_in_contact()):
		camera_reseting(delta)
	
	wanted_camera_rotation = CAR_STEERING_OFFSET_MAX * car.steer_val
	
	wanted_fov = car_velocity*0.15 + 80
	if fov > wanted_fov:
		wanted_fov -= MAX_FOV_STEPS
	elif fov < wanted_fov:
		wanted_fov += MAX_FOV_STEPS
	
	fov = wanted_fov

func camera_reseting(var delta):
	pivot.rotate_y(AUTO_SPEED_CAMERA_MOVEMENT*delta*(wanted_camera_rotation-pivot.get_rotation().y))

func camera_reset():
	pivot.rotation = Vector3(0,0,0)